
model = { 
    getStateUpdateFunctions: function () {
        var step1 = function(state) {
            state.x = 3
        }
        var step2 = function(state) {
            state.y = 5
        }
        var step3 = function(state) {
            state.result = (state.x * state.y)
        }
        var step4 = function(state) {
            state.y += 1
        }
        var step5 = function(state) {
            state.result *= state.x + state.y
        }

        return [step1, step2, step3, step4, step5]
    },

    initProgramState: function() {
        return {
            /// this is the actual state of the program being traced
            x: 0,
            y: 0,
            result: 0,
            /// this is the state of the program tracer
            stateXformers: model.getStateUpdateFunctions(),
            pc: 0, // program counter
            executeNextStatement: function() {
                this.stateXformers[this.pc](this)
                ++this.pc
            },
            toString: function() {
                return "{ x: " + this.x + ", y: " + this.y + ", result: " + this.result + " }"
            }
        }
    }
}

view = {
    shouldStop: false,
    timer: null,
    setTimer: function(callback, delay) {
        this.timer = window.setTimeout(callback.bind(this), delay)
    },
    clearTimer: function() {
        if (this.timer) { 
            window.clearTimeout(this.timer)
            this.timer = null
        }
    },

    currentStatement: null,
    highlightStatement: function() {
        if (view.shouldStop) return
        var stat = this.currentStatement
        if (stat.previousElementSibling) {
            stat.previousElementSibling.classList.remove('active');
        }
        stat.classList.add('active');
        console.log(stat.innerText.trim())
        var next_statement = stat.nextElementSibling
        if (next_statement) {
            this.currentStatement = next_statement
            view.setTimer(this.highlightStatement, 1000)
        }
    },

    run: function(stat_block_id) {
        var block = document.getElementById(stat_block_id)
        this.currentStatement = block.firstElementChild
        view.setTimer(this.highlightStatement, 200) 
    },

    onStartBoundToThis: null,
    onStopBoundToThis: null,

    onStart: function() {
        this.shouldStop = false
        this.clearTimer()
        var buttonRun = document.getElementById("run")
        buttonRun.innerText = "Stop"
        buttonRun.removeEventListener("click", this.onStartBoundToThis)
        buttonRun.addEventListener("click", this.onStopBoundToThis)
        view.run("statement_block")
    },

    onStop: function() {
        this.shouldStop = true
        this.clearTimer()
        this.currentStatement = null
        var buttonRun = document.getElementById("run")
        buttonRun.innerText = "Start"
        buttonRun.removeEventListener("click", this.onStopBoundToThis)
        var elems = document.getElementsByClassName("active")
        if (elems && elems.length) elems[0].classList.remove('active');        
        buttonRun.addEventListener("click", this.onStartBoundToThis)
    },

    init: function() {
        this.shouldStop = false
        var buttonRun = document.getElementById("run")
        this.onStartBoundToThis = this.onStart.bind(this)
        this.onStopBoundToThis = this.onStop.bind(this)
        buttonRun.addEventListener("click", this.onStartBoundToThis)
    }
}

window.addEventListener("load", window.view.init.bind(window.view))
