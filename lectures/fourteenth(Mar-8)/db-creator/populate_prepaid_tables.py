from gluon import dal

CONNECTION_STR = 'sqlite:://databases/wave.db'
TWO_G_CHARGES_TABLE = 'two_g_charges'
THREE_G_CHARGES_TABLE = 'three_g_charges'
FULL_TALKTIME_CHARGES_TABLE = 'full_talktime_charges'
AMOUNT_COL = 'amount'

TWO_G_CHARGES_DATA_CSV_PATH = 'databases/2g_charges.csv'
THREE_G_CHARGES_DATA_CSV_PATH = 'databases/3g_charges.csv'
FULL_TALKTIME_CHARGES_DATA_CSV_PATH = 'databases/full_talktime_charges.csv'

def importPrepaidCharges(table, csv_path):
    table.import_from_csv_file(open(csv_path, 'r'))

def populate_2g_charges():
    global CONNECTION_STR, TWO_G_CHARGES_DATA_CSV_PATH, TWO_G_CHARGES_TABLE, AMOUNT_COL
    db = dal.DAL(uri=CONNECTION_STR,  migrate=True)
    field = db.Field(AMOUNT_COL, type='integer', required=True, notnull=True, unique=True)
    table = db.define_table(TWO_G_CHARGES_TABLE, field, redefine=False)
    # check if the table is empty, and is so, populate it.
    # FYI, table.select() returns the rows in the table.
    rows = db(db.two_g_charges.amount).select()
    if len(rows) is 0:
        print "Populating 2G charges..."
        # import charges from the csv file
        importPrepaidCharges(db.two_g_charges, TWO_G_CHARGES_DATA_CSV_PATH)
        db.commit()


def populate_3g_charges():
    global CONNECTION_STR, THREE_G_CHARGES_DATA_CSV_PATH, THREE_G_CHARGES_TABLE, AMOUNT_COL
    db = dal.DAL(uri=CONNECTION_STR,  migrate=True)
    field = db.Field(AMOUNT_COL, type='integer', required=True, notnull=True, unique=True)
    table = db.define_table(THREE_G_CHARGES_TABLE, field, redefine=False)
    # check if the table is empty, and is so, populate it.
    # FYI, table.select() returns the rows in the table.
    rows = db(db.three_g_charges.amount).select()
    if len(rows) is 0:
        print "Populating 3G charges..."
        # import charges from the csv file
        importPrepaidCharges(db.three_g_charges, THREE_G_CHARGES_DATA_CSV_PATH)
        db.commit()


def populate_full_talktime_charges():
    global CONNECTION_STR, FULL_TALKTIME_CHARGES_DATA_CSV_PATH, FULL_TALKTIME_CHARGES_TABLE, AMOUNT_COL
    db = dal.DAL(uri=CONNECTION_STR,  migrate=True)
    field = db.Field(AMOUNT_COL, type='integer', required=True, notnull=True, unique=True)
    table = db.define_table(FULL_TALKTIME_CHARGES_TABLE, field, redefine=False)
    # check if the table is empty, and is so, populate it.
    # FYI, table.select() returns the rows in the table.
    rows = db(db.full_talktime_charges.amount).select()
    if len(rows) is 0:
        print "Populating full talktime charges..."
        # import charges from the csv file
        importPrepaidCharges(db.full_talktime_charges, FULL_TALKTIME_CHARGES_DATA_CSV_PATH)
        db.commit()


if __name__ == '__main__':
    populate_2g_charges()
    populate_3g_charges()
    populate_full_talktime_charges()

